const express = require('express');
const { v4: uuidv4 } = require('uuid');
const bodyparser = require('body-parser');
const _ = require('underscore');
const sessionSchema = require('../schemas/session');
const jwt = require('jsonwebtoken');
const jwtM = require('express-jwt');

module.exports = (config, messages, db) => {

    const sessionRouter = express.Router();

    sessionRouter.use(bodyparser.json());
    sessionRouter.use(bodyparser.urlencoded({ extended: false }));

    sessionRouter.route('/')

    .post(jwtM({ secret: config.jwtPassword, algorithms: ['RS256', 'HS256'] }), function(req, res) {

        const userR = req.user;
        if (!userR) {
            return res.status(401).send(messages.unauthorized_error);
        }

        const bodyReq = req.body;

        if (!bodyReq) {
            return res.status(400).send({ msg: messages.bad_request_msg });
        }

        // TODO: Fix Joi validation
        const { inputError, value } = sessionSchema.validate(bodyReq);
        if (inputError) {
            return res.status(400).send(inputError);
        }

        let sessions = db.get('sessions');

        if (!sessions || !Array.isArray(sessions)) {
            sessions = [];
        }

        const sessionToCreate = {
            id: uuidv4(),
            owner: userR.userid,
            username: userR.username,
            ...bodyReq
        };
        sessions.push(sessionToCreate);
        const newSess = db.set(sessions, "sessions");

        return res.status(201).send(sessionToCreate);

    })

    .get(jwtM({ secret: config.jwtPassword, algorithms: ['RS256', 'HS256'] }), function(req, res) {
        var userR = req.user;
        if (!userR) {
            return res.status(401).send(messages.unauthorized_error);
        }
        const sessions = db.get('sessions');
        return res.status(200).send((sessions || []).filter((s) => s.owner === userR.userid));
    });

    sessionRouter.route('/:id')

    .get(jwtM({ secret: config.jwtPassword, algorithms: ['RS256', 'HS256'] }), (req, res) => {

        var userR = req.user;
        if (!userR) {
            return res.status(401).send(messages.unauthorized_error);
        }

        const sessions = db.get('sessions');

        const existingSession = sessions.find((s) => s.id === req.params.id);
        if (!existingSession) {
            return res.status(404).send({ msg: 'Session not found' });
        }
        if (existingSession.owner !== userR.userid) {
            return res.status(401).send(messages.unauthorized_error);
        }
        return res.status(200).send(existingSession);
    })

    .put(jwtM({ secret: config.jwtPassword, algorithms: ['RS256', 'HS256'] }), (req, res) => {

        var userR = req.user;
        if (!userR) {
            return res.status(401).send(messages.unauthorized_error);
        }

        const sessions = db.get('sessions');
        let modifySession;
        const existingSession = sessions.find((s) => s.id === req.params.id);

        if (!existingSession) {
            return res.status(404).send({ msg: 'Session not found' });
        } else if (existingSession.owner !== userR.userid) {
            return res.status(401).send(messages.unauthorized_error);
        } else if (existingSession) {
            for (let session of sessions) {
                if (session.id === req.params.id) {
                    session.description = req.body.description
                    modifySession = session;
                }
            }
            db.update(sessions, "sessions");
            return res.status(200).send(modifySession);
        }


    })

    .delete(jwtM({ secret: config.jwtPassword, algorithms: ['RS256', 'HS256'] }), (req, res) => {

        var userR = req.user;
        if (!userR) {
            return res.status(401).send(messages.unauthorized_error);
        }

        let sessions = db.get('sessions');
        let newsessions = [];
        let existingSession = sessions.find((s) => { return s.id === req.params.id })


        if (!existingSession) {
            return res.status(404).send({ msg: 'Session not found' });
        } else if (existingSession.owner !== userR.userid) {
            return res.status(401).send(messages.unauthorized_error);
        } else if (existingSession) {
            for (let session of sessions) {
                if (session.id !== req.params.id) {
                    newsessions.push(session)
                }
            }
            const deletedSession = db.update(newsessions, "sessions")
            return res.status(200).send(newsessions);
        }


    });

    return sessionRouter;

}