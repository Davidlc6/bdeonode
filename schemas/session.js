const Joi = require('joi');

// TODO: Define session schema
module.exports = Joi.object({
    id: Joi.string()
        .required(),
    owner: Joi.string()
        .required(),
    username: Joi.string()
        .required(),
    description: Joi.string(),
    date: Joi.date()
        .required()
});