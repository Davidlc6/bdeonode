# Bdeo Testing API

This project implement a simple REST API for testing and practicing purposes.

## Scope

This API implements an in-memory database to store sessions belonging to users.
One user can only query its belonging sessions.
A user with administration role can list users and sessions.
This API also implements JWT based login.

## Runing the API

Once you have downloaded, cloned or forked the repo, you need to run the following commands:

* npm i (to install all the dependencies)
* npm start (to run the server API)

The API is serve locally in http://localhost:3000/api/v1

## API DOCS

We implement all this routes:

* GET /api/v1/ (ping method)
* POST /api/v1/user/login (obtains authorization token)
* POST /api/v1/user (register a new user)
* GET /api/v1/user (list registered users)
* GET /api/v1/user/:id (get user by id)
* POST /api/v1/session (register a new session)
* GET /api/v1/session (list registered sessions)
* GET /api/v1/session/:id (get session by id)

## TODO

* Test the API
* Validate schemas
* Implement additional routes
* Yaml documentation
